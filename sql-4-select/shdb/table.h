#pragma once

#include "bufferpool.h"
#include "row.h"

#include <functional>
#include <memory>

namespace shdb {

struct Page
{
    virtual RowIndex get_row_count() = 0;
    virtual Row get_row(RowIndex index) = 0;
    virtual void delete_row(RowIndex index) = 0;
    virtual std::pair<bool, RowIndex> insert_row(const Row &row) = 0;
};

struct Table
{
    virtual PageIndex get_page_count() = 0;
    virtual std::shared_ptr<Page> get_page(PageIndex page_index) = 0;
    virtual RowId insert_row(const Row &row) = 0;
    virtual Row get_row(RowId row_id) = 0;
    virtual void delete_row(RowId row_id) = 0;
};

using PageProvider = std::function<std::shared_ptr<Page>(std::shared_ptr<Frame>)>;

std::shared_ptr<Table>
    create_table(std::shared_ptr<BufferPool> buffer_pool, std::shared_ptr<File> file, PageProvider provider);

}    // namespace shdb
