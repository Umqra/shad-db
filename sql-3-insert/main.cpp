#include "shdb/db.h"
#include "shdb/sql.h"

#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <cassert>
#include <chrono>
#include <iostream>
#include <sstream>

auto fixed_schema = std::make_shared<shdb::Schema>(shdb::Schema{{"id", shdb::Type::uint64},
                                                                {"name", shdb::Type::varchar, 1024},
                                                                {"age", shdb::Type::uint64},
                                                                {"graduated", shdb::Type::boolean}});

std::shared_ptr<shdb::Database> create_database(int frame_count)
{
    auto db = shdb::connect("./mydb", frame_count);
    if (db->check_table_exists("test_table")) {
        db->drop_table("test_table");
    }
    db->create_table("test_table", fixed_schema);
    return db;
}

void populate(shdb::Sql &sql)
{
    sql.execute("DROP TABLE test_table");
    sql.execute("CREATE TABLE test_table (id uint64, age uint64, name string, girl boolean)");
    sql.execute("INSERT test_table VALUES (0, 10+10, \"Ann\", 1>0)");
    sql.execute("INSERT test_table VALUES (1, 10+10+1, \"Bob\", 1<0)");
    sql.execute("INSERT test_table VALUES (2, 10+9, \"Sara\", 1>0)");
}

void test_insert()
{
    auto db = create_database(1);
    auto sql = shdb::Sql(db);
    populate(sql);

    auto rows =
        std::vector<shdb::Row>{{static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true},
                               {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
                               {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true}};

    size_t index = 0;
    auto table = db->get_table("test_table");
    auto scan = shdb::Scan(table);
    for (auto it = scan.begin(), end = scan.end(); it != end; ++it) {
        auto row = it.get_row();
        if (!row.empty()) {
            assert(row == rows[index]);
            ++index;
        }
    }

    std::cout << "Test insert passed" << std::endl;
}

void cmd()
{
    auto db = shdb::connect("./mydb", 1);
    auto sql = shdb::Sql(db);

    while (!std::cin.eof()) {
        std::cout << "shdb> " << std::flush;
        std::string line;
        std::getline(std::cin, line);
        if (!line.empty()) {
            try {
                auto rowset = sql.execute(line);
                for (auto *row : rowset.rows) {
                    std::cout << to_string(*row) << std::endl;
                }
            } catch (char const *ex) {
                std::cout << "Error: " << ex << std::endl;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    test_insert();
}
