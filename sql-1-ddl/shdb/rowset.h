#pragma once

#include "row.h"
#include "schema.h"

#include <memory>

namespace shdb {

struct Rowset
{
    std::shared_ptr<shdb::Schema> schema;
    std::vector<shdb::Row *> rows;

    Rowset();
    Rowset(std::shared_ptr<shdb::Schema> schema);
    Rowset(const Rowset &) = delete;
    Rowset(Rowset &&other);
    ~Rowset();

    shdb::Row *allocate();
};

}    // namespace shdb
